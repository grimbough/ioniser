Package: IONiseR
Title: Quality Assessment Tools for Oxford Nanopore MinION data
Version: 0.99.4
Authors@R: person("Mike", "Smith", email = "grimbough@gmail.com",
                  role = c("aut", "cre"))
Description: IONiseR provides tools for the quality assessment of Oxford Nanopore MinION data.  
    It extracts summary statistics from a set of fast5 files and can be used either before or
    after base calling.  In addition to standard summaries of the read-types produced, it provides 
    a number of plots for visualising metrics relative to experiment run time or spatially over 
    the surface of a flowcell. 
License: MIT + file LICENSE
Depends: R (>= 3.2)
Imports: rhdf5, dplyr, magrittr, tidyr, data.table, ShortRead, Biostrings, 
    ggplot2, methods, BiocGenerics, XVector
VignetteBuilder: knitr
Suggests: BiocStyle, knitr, rmarkdown, gridExtra,
    testthat, minionSummaryData
biocViews: QualityControl, DataImport, Sequencing

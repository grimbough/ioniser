# README #

IONiseR is an R package for quality assessment of data from Oxford Nanopore's MinION sequencer.

This repository is currently a dumping ground for related code, but the R package itself can be found in the IONiseR subdirectory.

Feedback and questions gratefully received at mike.smith@cruk.cam.ac.uk




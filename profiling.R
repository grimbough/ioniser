

## trying to understand why I see few channels than if you just use the read names


source("/Users/smith68/Code/IONiseR/extractors.R")
source("/Users/smith68/Code/IONiseR/readData.R")
source("/Users/smith68/Code/IONiseR/plotLayout.R")
setwd("/Users/smith68/Data/Minion/Lambda burn-in data/Met v1.10/")
files <- list.files(pattern = ".fast5$")

channelsFromFile <- sapply(files, .getReadChannelMux())
channelsFromFile <- rbindlist(channelsFromFile)

channelsFromNames <- as.integer(substring(do.call("rbind", strsplit(files, "_"))[,3], 3))


library(lineprof)
file <- "/Users/smith68/Data/Minion//Ecoli_R73/LomanLabz_PC_Ecoli_K12_R7.3_2549_1_ch1_file1_strand.fast5"

fun1 <- function(file) {
  fid <- H5Fopen(file)
  gid <- H5Gopen(fid, "/Analyses/EventDetection_000/Reads")
  read_number_char <- h5ls(gid)[1,"name"]
  H5Gclose(gid)
  H5Fclose(fid)
  attrs <- h5readAttributes(file, paste0("/Analyses/EventDetection_000/Reads/", read_number_char))
  return(c(attrs$start_mux, attrs$read_number))
}

fun2 <- function(file) {
  fid <- H5Fopen(file)
  on.exit(H5Fclose(fid))
  
  gid <- H5Gopen(fid, "/Analyses/EventDetection_000/Reads")
  read_number_char <- h5ls(gid)[1,"name"]
  H5Gclose(gid)

  gid <- H5Gopen(fid, paste0("/Analyses/EventDetection_000/Reads/", read_number_char))
  
  aid <- H5Aopen(gid, "start_mux")
  start_mux <- H5Aread(aid)
  H5Aclose(aid)
  aid <- H5Aopen(gid, "read_number")
  read_number <- H5Aread(aid)
  H5Aclose(aid)
  
  H5Gclose(gid)
  return(c(start_mux, read_number))
  
}

files <- list.files(path = "/Users/smith68/Data/Minion/Ecoli_R73/", pattern = ".fast5$", full.names = TRUE)

system.time(tmp1 <- lapply(files[1:100], fun1))
system.time(tmp2 <- lapply(files[1:100], fun2))



  

kmerHeatmap <- function(summaryData, kmerLength = 5, hours = NULL) {
  
  hour <- summaryData@baseCalled[,start_time] %/% (3600)
  if(!is.null(hours)) {
    hoursIdx <- which(hour %in% hours)
    hour <- hour[hoursIdx]
  } else {
    hoursIdx <- 1:length(hour)
  }
  
  fastq.tmp <- summaryData@fastq[hoursIdx]
  kmers <- oligonucleotideFrequency(x = sread(fastq.tmp), width = kmerLength)
  
  tmp <- data.table(kmers, hour) %>% 
    gather(kmers, count, 1:4^kmerLength) %>%
    group_by(hour, kmers) %>% 
    summarise(total_hour_count = sum(count)) %>% 
    arrange(hour) 
  
  tmp2 <- group_by(tmp, hour) %>% 
    summarise(all_kmers = sum(total_hour_count)) 
  
  tmp3 <- right_join(tmp, tmp2, by = "hour") %>%
    mutate(kmer_frac = total_hour_count / all_kmers) %>%
    select(hour, kmers, kmer_frac)
  tmp3 <- data.table(tmp3)
  
  tmp4 <- group_by(tmp3, kmers) %>% mutate(scaled = rescale(kmer_frac))
  
  kmerord <- data.table(kmers = levels(tmp4[,kmers]), ord = 
                          order(order(letterFrequency(DNAStringSet(levels(tmp4[,kmers])), 'CG')))
  )
  tmp4 <- left_join(tmp4, kmerord, by = 'kmers')
  
  #ggplot(tmp4, aes(x = hour, y = ord, fill = kmer_frac)) + 
  ggplot(tmp4, aes(x = hour, y = ord, fill = scaled)) +
    geom_raster() +
    scale_fill_gradient(limits = quantile(tmp4[,scaled], probs = c(0.02, 0.98)),
                        low = "darkblue", high = "green") +
    scale_x_continuous(expand = c(0, 0)) +
    scale_y_continuous(expand = c(0, 0))
}


IDX2d <- grep('2D', id(fastq(summaryData)))
fastq.tmp <- fastq(summaryData)[IDX2d]
kmers <- oligonucleotideFrequency(x = sread(fastq.tmp), width = 5, as.prob = TRUE)

kmers2 <- gather(data.table(kmers, 
                            filter(baseCalled(summaryData), strand == 'template', full_2D == TRUE) %>%
                                select(id) %>% 
                                inner_join(rawData(summaryData), by = "id")), 
                 key = "kmer",  value = "count", AAAAA:TTTTT)

kmers2 %>% group_by(kmer, time = start_time %/% 900) %>% summarise(count = median(count))

tmp <- left_join(kmers2 %>% group_by(kmer, time = start_time %/% 900) %>% summarise(count = median(count)),
          kmers2 %>% group_by(time = start_time %/% 900) %>% summarise(med_sig = median(median_signal)),
          by = "time")


ggplot(kmers2, aes(y = count, x = factor(kmer))) + geom_boxplot(fill = "orange", outlier.size = 0) + ylim(0,50)


## kmer histograms
mat <- matrix(nrow = 256, ncol = 10)
for(i in 1:10) {
fq.tmp <- fastq2D(uea2b)[sample(1:24397, 1000, replace = FALSE)]
mat[,i] <- colSums(oligonucleotideFrequency(x = sread(fq.tmp), width = 4, as.prob = FALSE)) / sum(width(fq.tmp))
}
for(i in 1:9) { plot(mat[,i], mat[,(i+1)]); abline(0,1, col = "red") }

#### this looks only at the restarted data mentioned in an email by Matt Loose
### Don't delete it!!!!

mat <- matrix(nrow = 1024, ncol = 3)

restart2d <- IONiseR:::.get2D(restart)
idx <- which(rawData(restart2d)[,start_time] < 7000)
#idx <- sample(idx, size = 276)
fq.tmp <- fastq2D(restart2d)[idx]
mat[,1] <- colSums(oligonucleotideFrequency(x = sread(fq.tmp), width = 5, as.prob = FALSE)) / sum(width(fq.tmp))
region <- rep("start", length(idx))
gc_content <- letterFrequency(sread(fq.tmp), as.prob = TRUE, letters = 'CG')

idx <- which(rawData(restart2d)[,start_time] >= 7000 & rawData(restart2d)[,start_time] < 40000)
#idx <- sample(idx, size = 276)
fq.tmp <- fastq2D(restart2d)[idx]
mat[,2] <- colSums(oligonucleotideFrequency(x = sread(fq.tmp), width = 5, as.prob = FALSE)) / sum(width(fq.tmp))
region <- c(region, rep("middle", length(idx)))
gc_content <- c(gc_content, letterFrequency(sread(fq.tmp), as.prob = TRUE, letters = 'CG'))

idx <- which(rawData(restart2d)[,start_time] >= 14000 & rawData(restart2d)[,start_time] < 40000)
fq.tmp <- fastq2D(restart2d)[idx]
mat[,3] <- colSums(oligonucleotideFrequency(x = sread(fq.tmp), width = 5, as.prob = FALSE)) / sum(width(fq.tmp))
region <- c(region, rep("end", length(idx)))
gc_content <- c(gc_content, letterFrequency(sread(fq.tmp), as.prob = TRUE, letters = 'CG'))

colnames(mat) <- c("start", "middle", "end")
mat <- data.table(mat)
gc_content <- data.table(region, gc_content)

p1 <- ggplot(NULL) + 
    geom_rect(aes(xmin = 0, xmax = 7000/3600, ymin = 45, ymax = 105), fill = "#FF6B6B", alpha = 0.5) +
    geom_rect(aes(xmin = 7000/3600, xmax = 14000/3600, ymin = 45, ymax = 105), fill = "#4EBABA", alpha = 0.5) +
    geom_rect(aes(xmin = 14000/3600, xmax = 40000/3600, ymin = 45, ymax = 105), fill = "#5CDD5C", alpha = 0.5) +
    geom_point(data = rawData(restart2d), mapping = aes(x = start_time / 3600, y = median_signal), alpha = 0.5) +
    geom_vline(xintercept = c(7000/3600, 14000/3600, 40000/3600), colour="#E97E25", linetype = "longdash", size = 1.5) +
    xlab("sequencing start time (hours)") +
    ylab("median signal (pA)") +
    ggtitle("Recorded current over time")
p2 <- ggplot(gc_content, aes(x = factor(region, levels = c("start", "middle", "end")), y = gc_content)) + 
    geom_boxplot(fill = c("#FF6B6B", "#4EBABA", "#5CDD5C"), alpha = 0.5) +
    xlab("region") +
    ggtitle("GC content")
p3 <- ggplot(mat, aes(x = start, y = middle)) + 
    geom_point() +
    geom_abline(intercept = 0, slope = 1, col = "#E97E25", linetype = "longdash", size = 2) +
    ggtitle("pentamer proportion\nstart vs middle")
p4 <- ggplot(mat, aes(x = start, y = end)) + 
    geom_point() +
    geom_abline(intercept = 0, slope = 1, col = "#E97E25", linetype = "longdash", size = 2) +
    ggtitle("pentamer proportion\nstart vs end")

grid.arrange(p1, p2,p3, p4, ncol = 2)



################################

compareKmerHours <- function(summaryData, hour1 = 1, hour2 = 2) {
    hour <- summaryData@baseCalled[,start_time] %/% (60*60)
    hourIdx1 <- which(hour %in% hour1)
    hourIdx2 <- which(hour %in% hour2)

    mat <- matrix(ncol = 2, nrow = 1024)
    colnames(mat) <- c("hour1", "hour2")
    fq.tmp <- fastq(summaryData)[hourIdx1]
    mat[,1] <- colSums(oligonucleotideFrequency(x = sread(fq.tmp), width = 5, as.prob = FALSE)) / sum(width(fq.tmp))
    fq.tmp <- fastq(summaryData)[hourIdx2]
    mat[,2] <- colSums(oligonucleotideFrequency(x = sread(fq.tmp), width = 5, as.prob = FALSE)) / sum(width(fq.tmp))
    mat <- data.table(mat)
    
    ggplot(mat, aes(x = hour1, y = hour2)) + 
        geom_point() +
        geom_abline(intercept = 0, slope = 1, col = "#E97E25", linetype = "longdash", size = 2) +
        ggtitle(paste("pentamer proportion - hour 1 vs hour", hour2, "\ncorrelation", cor(mat)[2])) +
        theme(plot.margin=unit(c(0.1,0.2,0.1,0.2), "cm"))
}

## compute correlation between pentemer proportions in two time windows
kmerFrequencyCorrelation <- function(summaryData, groupedMinutes = 10, only2D = TRUE) {
    
    if(only2D) {
        idx <- grep('2D', id(fastq(summaryData)))
    } else {
        idx <- 1:nrow(baseCalled(summaryData))
    }
    pentamers <- oligonucleotideFrequency(x = sread(fastq(summaryData)[idx]), width = 5, as.prob = TRUE)
    
    if(only2D) {
        tmp <- data.table(filter(baseCalled(summaryData), strand == "template", full_2D == TRUE), pentamers)
    } else {
        tmp <- data.table(baseCalled(summaryData), pentamers)
    }
    
    tmp2 <- group_by(tmp, time_group = start_time %/% (60 * groupedMinutes)) %>%
        summarise_each(funs(mean), AAAAA:TTTTT) %>%
        arrange(time_group)
    
    tmp3 <- gather(tmp2, key = "pentamer", value = "freq", AAAAA:TTTTT) %>%
        spread(key = time_group, value = freq) %>% 
        select(-pentamer)
    
    correlations <- cor(as.matrix(tmp3))
    
    hours <- (as.integer(colnames(correlations)) * groupedMinutes) / 60
    tmp5 <- data.table(x = rep(hours, each = length(hours)), y = rep(hours, length(hours)), cor = as.vector(correlations))
    
    ggplot(tmp5, aes(x = x, y = y, fill = cor)) + 
        geom_raster() +
        scale_x_continuous(expand = c(0, 0)) + 
        scale_y_continuous(expand = c(0, 0)) +
        xlab("hour") +
        ylab("hour")
}



plotTmp <- function(summaryData) {
p1 <- IONiseR:::channelActivityPlot(summaryData, select(rawData(summaryData), id, median_signal)) +
    scale_fill_continuous(high = "#E97E25", low="darkblue") +
    scale_colour_continuous(high = "#E97E25", low="darkblue")
p2 <- kmerCorrelation(summaryData, groupedMinutes = 30) + 
    scale_fill_continuous(high = "#E97E25", low="darkblue", limits = c(0.5,1))
grid.arrange(p1, p2)
}



compareHours <- function(summaryData, hour1 = 0, hour2 = 1, model = model) {

  hour <- summaryData@baseCalled[,start_time] %/% (60*60)
  hourIdx <- which(hour %in% c(hour1, hour2))
  
  fastq.tmp <- summaryData@fastq[hourIdx,]
  kmers <- oligonucleotideFrequency(x = sread(fastq.tmp), width = 5)

  tmp <- data.table(kmers, hour = hour[hourIdx]) %>% 
          gather(kmers, count, AAAAA:TTTTT) %>%
          group_by(hour, kmers) %>% 
          summarise(total_hour_count = sum(count)) %>% 
          arrange(hour) 
  
  tmp2 <- group_by(tmp, hour) %>% 
          summarise(all_kmers = sum(total_hour_count)) 
  
  tmp3 <- right_join(tmp, tmp2, by = "hour") %>%
          mutate(kmer_frac = total_hour_count / all_kmers) %>%
          select(hour, kmers, kmer_frac)
  tmp3 <- data.table(tmp3)
  
  test <- data.table(inner_join(filter(tmp3, hour == hour1), filter(tmp3, hour == hour2), by = "kmers"), gc_content = letterFrequency(BStringSet(as.character(test[,kmers])), letters = c("C|G"))[,1], model_mean = model[,"level_mean"])
  
  ggplot(test, aes(x = kmer_frac.x, y = kmer_frac.y, colour = model_mean)) + 
    geom_point(size = 2) + 
    geom_abline(intercept = 0, slope = 1) + 
    scale_colour_gradient(low = "darkblue", high = "green") +
    xlab(paste("hour", hour1)) +
    ylab(paste("hour", hour2))

}

      
